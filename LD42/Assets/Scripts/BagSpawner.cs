﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BagSpawner : MonoBehaviour {

    public List<GameObject> inside = new List<GameObject>();

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if (inside.Count == 0)
        {
            GameObject go = Instantiate(Resources.Load<GameObject>("Bag"));
            inside.Add(go);
            go.transform.position = transform.position;
        }
	}

    void OnTriggerEnter(Collider collision)
    {
        if (!inside.Contains(collision.gameObject) && collision.GetComponent<Item>())
        {
            inside.Add(collision.gameObject);
        }
    }

    void OnTriggerExit(Collider collision)
    {
        if (inside.Contains(collision.gameObject))
        {
            inside.Remove(collision.gameObject);
        }
    }
}
