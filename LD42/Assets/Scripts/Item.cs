﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Item : MonoBehaviour {

    public enum ItemTypes
    {
        BAG, BREAD, CAKE, SALAD, BUTTER, CAN, CEREAL
    }

    public ItemTypes itemType;

    float followForce = 20f;

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    private void OnMouseDrag() {
        Vector3 mousePos = Input.mousePosition;
        mousePos.z = -Camera.main.transform.position.z;
        mousePos = Camera.main.ScreenToWorldPoint(mousePos);
        GetComponent<Rigidbody>().AddForce((mousePos - GetComponent<Transform>().position) * followForce);
    }
}
