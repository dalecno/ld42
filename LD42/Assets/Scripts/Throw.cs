﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Throw : MonoBehaviour {

    public Vector2 throwForce;

    public List<GameObject> throwList = new List<GameObject>();

    public float throwTimeMin;
    public float throwTimeMax;
    private float nextThrow = 0;

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void FixedUpdate () {
        if (Time.timeSinceLevelLoad >= nextThrow && throwList.Count > 0) {
            GameObject go = Instantiate(throwList[0]);
            throwList.RemoveAt(0);
            go.transform.position = transform.position;
            go.GetComponent<Rigidbody>().velocity = throwForce;

            nextThrow = Time.timeSinceLevelLoad + Random.Range(throwTimeMin, throwTimeMax);
            transform.parent.parent.Find("Employee").GetComponent<Animator>().Play("ArmThrower");
        }
	}
}
