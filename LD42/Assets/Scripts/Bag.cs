﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bag : MonoBehaviour {

    public List<GameObject> inside = new List<GameObject>();
    private GameObject orderHandlerGO;
    private OrderHandling orderHandler;
    private GameObject collector;

	// Use this for initialization
	void Start () {
        orderHandler = OrderHandling.orderHandler;
        orderHandlerGO = orderHandler.gameObject;
        collector = orderHandlerGO.GetComponentInChildren<Collector>().gameObject;
	}
	
	// Update is called once per frame
	void Update () {
	}

    bool fix = false;
    void OnTriggerEnter(Collider collision) {
        if (fix) return;
        fix = true;
        if (!inside.Contains(collision.gameObject) && collision.GetComponent<Item>()) {
            inside.Add(collision.gameObject);
        } else if (collision.gameObject.Equals(collector)) {
            Score.score.addScore(-1);
            if (orderHandler.orders.Count == 0) return;
            Order o = orderHandler.orders.ToArray()[0];
            for (int i = 0; i < inside.Count; i++) {
                GameObject insideGO = inside.ToArray()[i];
                if (o.order.Contains(insideGO.GetComponent<Item>().itemType)) {
                    o.order.Remove(insideGO.GetComponent<Item>().itemType);
                    inside.Remove(insideGO);
                    Score.score.addScore(1);
                    Destroy(insideGO);
                    if (i != inside.Count) i--;
                    else break;
                } else {
                    o.order.Remove(insideGO.GetComponent<Item>().itemType);
                    inside.Remove(insideGO);
                    Score.score.addScore(-1);
                    Destroy(insideGO);
                    if (i != inside.Count) i--;
                    else break;
                }
            }
            orderHandler.updateUI();
            Destroy(gameObject.transform.parent.gameObject);
        }
        fix = false;
    }

    void OnTriggerExit(Collider collision) {
        if (inside.Contains(collision.gameObject))
        {
            inside.Remove(collision.gameObject);
        }
    }
}
