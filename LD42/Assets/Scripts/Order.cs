﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Order {

    public List<Item.ItemTypes> order = new List<Item.ItemTypes>();

    public Order(Item.ItemTypes[] go) {
        order.AddRange(go);
    }
}
