﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Score : MonoBehaviour {

    public static Score score;
    public static Score scoreHandler;
    private int curScore = 0;

	// Use this for initialization
	void Start () {
        if (!GetComponent<Text>())
        {
            DontDestroyOnLoad(this.gameObject);
            scoreHandler = this;
            print("ScoreHandler created");
            if (score) score.addScore(0);
        } else
        {
            score = this;
            print("Score created");
            if (scoreHandler) score.addScore(0);
        }
	}
	
	// Update is called once per frame
	void Update () {
		
	}
     
    public void addScore(int add)
    {
        scoreHandler.curScore += add;
        GetComponent<Text>().text = "Score: " + scoreHandler.curScore;
    }

    public void setScore(int newScore)
    {
        scoreHandler.curScore = newScore;
    }

    public void loadScene(int num)
    {
        SceneManager.LoadScene(num);
    }
}
