﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class OrderHandling : MonoBehaviour {

    public GameObject[] posItems;

    public List<Order> orders = new List<Order>();

    public Canvas canvas;

    private float nextOrder = 0;

    private Throw thrower;

    public int[] UIAmounts;

    private GameObject[] canvasChildren;

    public static OrderHandling orderHandler;

    private void Awake()
    {
        orderHandler = this;
    }

    // Use this for initialization
    void Start () {
        foreach (Transform child in transform)
        {
            if (child.GetComponent<Throw>())
            {
                thrower = child.GetComponent<Throw>();
                break;
            }
        }

        canvasChildren = new GameObject[canvas.transform.childCount];
        for (int i = 0; i < canvasChildren.Length; i++)
        {
            canvasChildren[i] = canvas.transform.GetChild(i).gameObject;
        }
        print(canvasChildren.Length);
        print(canvasChildren[0]);

        posItems = Resources.LoadAll<GameObject>("Items");
    }

	// Update is called once per frame
	void Update () {
		if (Time.timeSinceLevelLoad >= nextOrder) {
            newOrder();
            nextOrder = Time.timeSinceLevelLoad + Random.Range(10, 20);
            print("New Order");
            foreach (Item.ItemTypes I in orders.ToArray()[orders.Count - 1].order) {
                print(I);
            }
        }
        if (orders.Count != 0) {
            if (orders.ToArray()[0].order.Count == 0) {
                print("kAGE");
                transform.parent.Find("CustomerOne").GetComponent<Animator>().Play("HeadWhopping");
                orders.Remove(orders.ToArray()[0]);
                updateUI();
            }
        }
    }

    void newOrder() {
        Item.ItemTypes[] items = new Item.ItemTypes[Random.Range(2, 5)];
        for (int i = 0; i < items.Length; i++) {
            items[i] = posItems[Random.Range(0, posItems.Length)].GetComponent<Item>().itemType;
        }
        orders.Add(new Order(items));

        foreach (Item.ItemTypes it in items) {
            for (int i = 0; i < posItems.Length; i++) {
                GameObject go = posItems[i];
                if (posItems[i].GetComponent<Item>().itemType.Equals(it)) {
                    thrower.throwList.Add(go);
                }
            }
        }
        updateUI();
    }

    public void updateUI() {
        //int count = 0;
        int num = 0;
        //count = orders.ToArray()[0].order.FindAll(o => o.Equals(Item.ItemTypes.BREAD)).Count;
        //if (count != 0)
        //{
        //    canvasChildren[num].transform.GetChild(0).gameObject.SetActive(true);
        //    canvasChildren[num].transform.GetChild(0).GetChild(0).GetComponent<Text>().text = count + "x";
        //    canvasChildren[num].transform.GetChild(0).GetChild(1).GetComponent<Image>().sprite = Resources.Load<Sprite>("Items/Bread");
        //    num++;
        //}
        if (orders.Count == 0) return;
        num += countNDoStuff(Item.ItemTypes.BREAD, Resources.Load<Sprite>("Items/Bread"), num);
        num += countNDoStuff(Item.ItemTypes.CAKE, Resources.Load<Sprite>("Items/Cake"), num);
        num += countNDoStuff(Item.ItemTypes.BUTTER, Resources.Load<Sprite>("Items/Butter"), num);
        num += countNDoStuff(Item.ItemTypes.SALAD, Resources.Load<Sprite>("Items/Salad"), num);
        num += countNDoStuff(Item.ItemTypes.CAN, Resources.Load<Sprite>("Items/Can"), num);
        num += countNDoStuff(Item.ItemTypes.CEREAL, Resources.Load<Sprite>("Items/Cereal"), num);

        for (int i = num; i < 6; i++)
        {
            canvasChildren[i].transform.GetChild(0).gameObject.SetActive(false);
        }
        print(orders.ToArray()[0].order.Count);
    }

    int countNDoStuff(Item.ItemTypes itemType, Sprite sprite, int num){
        int count = orders.ToArray()[0].order.FindAll(o => o.Equals(itemType)).Count;
        if (count != 0)
        {
            canvasChildren[num].transform.GetChild(0).gameObject.SetActive(true);
            canvasChildren[num].transform.GetChild(0).GetChild(0).GetComponent<Text>().text = count + "x";
            canvasChildren[num].transform.GetChild(0).GetChild(1).GetComponent<Image>().sprite = sprite;
            return 1;
        }
        return 0;
    }
}
